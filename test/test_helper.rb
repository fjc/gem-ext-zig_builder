# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)
require 'gem/ext/zig_builder'

require 'minitest/autorun'
