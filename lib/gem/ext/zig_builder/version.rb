# frozen_string_literal: true

module Gem
  module Ext
    class Builder; end

    class ZigBuilder < Builder
      VERSION = '0.4.3'
    end
  end
end
