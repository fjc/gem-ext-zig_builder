# frozen_string_literal: true

require_relative 'zig_builder/version'
require 'rubygems/ext/builder'
require 'rbconfig'
require 'tmpdir'

# This class is used by rubygems to build Zig extensions. It is a thin-wrapper
# over the zig build command. To trigger this builder, specify a build.zig
# file in the gem spec.
#
# A minimal zig.build example:
#
#   const std = @import("std");
#   pub fn build(b: *std.Build) void {
#       const optimize = b.standardOptimizeOption(.{});
#       const target = b.standardTargetOptions(.{});
#       const lib = b.addSharedLibrary(.{
#           .name = "zigrb_example",
#           .root_source_file = b.path("src/zigrb.zig"),
#           .target = target,
#           .optimize = optimize,
#       });
#       lib.linkSystemLibrary(std.posix.getenv("RUBY_PC") orelse "ruby");
#       lib.linkSystemLibrary("c");
#       b.installArtifact(lib);
#
#       const unit_tests = b.addTest(.{
#           .root_source_file = b.path("src/zigrb.zig"),
#           .target = target,
#           .optimize = optimize,
#       });
#       unit_tests.linkSystemLibrary(std.posix.getenv("RUBY_PC") orelse "ruby");
#       unit_tests.linkSystemLibrary("c");
#       const run_unit_tests = b.addRunArtifact(unit_tests);
#       const test_step = b.step("test", "Run unit tests");
#       test_step.dependOn(&run_unit_tests.step);
#   }
#
# A minimal zigrb.zig example:
#
#   const std = @import("std");
#   const ruby = @cImport(@cInclude("ruby/ruby.h"));
#
#   // Export the extension init function:
#   export fn Init_libzigrb_example() void {
#       ruby.ruby_init();
#   }
#
#   // For development releases of ruby:
#   const rubyABIVersion = if (@hasField(ruby, "RUBY_ABI_VERSION")) ruby.RUBY_ABI_VERSION else 0;
#   export fn ruby_abi_version() ruby.VALUE { return rubyABIVersion; }
#
# An example of defining a ruby class and adding a zig method:
#
#   const zig_rb_class: ruby.VALUE = ruby.rb_define_class("ZigExample", ruby.rb_cObject);
#   _ = ruby.rb_define_method(zig_rb_class, "method_name", rb_example_method, 2);
#
# An example of a zig ruby method:
#
#   fn rb_example_method(...) callconv(.C) ruby.VALUE {
#       var ap = @cVaStart();
#       defer @cVaEnd(&ap);
#       // first argument is `self` in ruby; discard it
#       const self = @cVaArg(&ap, ruby.VALUE); _ = self;
#       // additional arguments
#       const _ = @cVaArg(&ap, ruby.VALUE);
#       const _ = @cVaArg(&ap, ruby.VALUE);
#       return ruby.QNil
#   }
#
# An example of ruby zig type conversions:
#
#   const m = ruby.NUM2INT(@cVaArg(&ap, ruby.VALUE));
#   const n = ruby.NUM2INT(@cVaArg(&ap, ruby.VALUE));
#   return ruby.INT2NUM(f(m, n));
#
class Gem::Ext::ZigBuilder < Gem::Ext::Builder
  #
  # This module patches the Builder builder_for method to ensure that
  # this builder class is used for build.zig extensions.
  #
  module PatchBuilderLookup
    def builder_for(extension)
      /build.zig/ === extension ? Gem::Ext::ZigBuilder : super
    end
  end

  class << self
    def build(_extension, dest_path, results, _args = [], _lib_dir = nil, extension_dir = nil, _target_rbconfig = nil)
      dlext   = RbConfig::CONFIG['DLEXT']
      tmp_dir = Dir.mktmpdir
      command = _build_command(tmp_dir)
      envvars = _build_envvars

      _run(command, results, 'zig', extension_dir, envvars)
      _install_artifacts(tmp_dir, dlext, dest_path)

      results
    ensure
      FileUtils.remove_entry tmp_dir if tmp_dir
    end

    private

    def _build_command(tmp_dir)
      [].tap { |c|
        c << 'zig' << 'build' << '-Doptimize=ReleaseSafe'
        c << '--prefix-lib-dir' << tmp_dir
        c << 'test' << 'install'
      }
    end

    def _build_envvars
      {
        'PKG_CONFIG_PATH' => "#{RbConfig::CONFIG['libdir']}/pkgconfig",
        'RUBY_PC'         => File.basename(RbConfig::CONFIG['ruby_pc'], '.pc'),
      }
    end

    def _run(command, results, command_name, dir, env)
      # old versions do not support the 4th `dir` arg
      # old versions do not support the 5th `env` arg
      case method(:run).parameters.count
      when 3
        _pollute_env(env)
        run(command, results, command_name)
      when 4
        _pollute_env(env)
        run(command, results, command_name, dir)
      when 5
        run(command, results, command_name, dir, env)
      else
        raise ArgumentError, 'wrong number of arguments'
      end
    end

    def _pollute_env(env)
      env.each do |k, v| ENV[k] = v end
    end

    def _install_artifacts(tmp_dir, dlext, dest_path)
      target_vendor   = RbConfig::CONFIG['target_vendor']

      Dir.chdir tmp_dir do
        if target_vendor == 'apple' && dlext == 'bundle'
          # zig builds a .dylib but ruby wants to load a .bundle
          Dir.glob('*.dylib').each do |dylib|
            FileUtils.mv dylib, "#{File.basename(dylib, '.dylib')}.bundle"
          end
        end

        FileUtils.mv Dir.glob("*.#{dlext}"), dest_path
      end
    end
  end
end

class Gem::Ext::Builder
  prepend Gem::Ext::ZigBuilder::PatchBuilderLookup
end
