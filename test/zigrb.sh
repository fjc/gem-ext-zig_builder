#!/bin/sh
set -ex

ZIG="${1:-".*-dev"}"

case "$(uname -s)" in
Linux)  OSA="linux-$(uname -m)" ;;
Darwin) OSA="macos-$(uname -m)" ;;
*) echo "unknown kernel" ; exit ;;
esac

curl $(curl https://ziglang.org/download/index.json | grep -e "$OSA-$ZIG" | sed 's;.*"\(https://.*\).*",;\1;' | head -1) | xzcat | tar -C /usr/local/bin --strip-components=1 -xf -
zig version
ruby -v

rake install
gem install test/zigrb/pkg/zigrb-0.0.1.gem

[ $(ruby -rlibzigrb -e 'p ZigRb.new.add(2, 2)') -eq 4 ] || false
