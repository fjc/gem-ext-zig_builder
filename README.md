# Gem::Ext::ZigBuilder

A gem extension builder for zig.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'gem-ext-zig_builder'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install gem-ext-zig_builder

## Usage

1. Install the gem
2. The zig builder plugin will be auto-loaded by the gem command
3. Use rubygems to install a zig extension

### Example

$ gem install --user pkg/zig_example-0.4.0.pre.gem 
Building native extensions. This could take a while...
ERROR:  Error installing pkg/zig_example-0.4.0.pre.gem:
	ERROR: Failed to build gem native extension.
    No builder for extension 'ext/zigrb_100doors/build.zig'

$ gem install --user gem-ext-zig_builder
Fetching gem-ext-zig_builder-0.3.0.gem
Successfully installed gem-ext-zig_builder-0.3.0
1 gem installed

$ gem install --user pkg/zig_example-0.4.0.pre.gem 
Building native extensions. This could take a while...
Successfully installed zig_example-0.4.0.pre
1 gem installed

$ ruby -v -rzig_example -e 'p [ZigExample::VERSION, ZigExample.new.lucas_lehmer(11), ZigExample.new.lucas_lehmer(13), ZigExample.new.hundred_doors(50)]'
ruby 3.2.1 (2023-02-08 revision 31819e82c8) +jemalloc [x86_64-linux]
["0.4.0.pre", false, true, 54]

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
