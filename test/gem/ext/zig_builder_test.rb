# frozen_string_literal: true

require 'test_helper'

class Gem::Ext::ZigBuilderTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Gem::Ext::ZigBuilder::VERSION
  end

  def test_patch_builder_lookup
    item = ::Gem::Ext::ZigBuilder.allocate

    eval 'class ::Gem::Ext::ExtConfBuilder < ::Gem::Ext::Builder; end'

    assert_equal(::Gem::Ext::ExtConfBuilder, item.builder_for('extconf.rb'))
    assert_equal(::Gem::Ext::ZigBuilder,     item.builder_for('build.zig'))
  end
end
