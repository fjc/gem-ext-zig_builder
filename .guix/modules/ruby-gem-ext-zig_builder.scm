(define-module (ruby-gem-ext-zig_builder)
  #:use-module (guix)
  #:use-module (guix git-download)
  #:use-module (guix build-system ruby)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages zig)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 textual-ports))

(define-public ruby-gem-ext-zig_builder
  (package
   (name "ruby-gem-ext-zig_builder")
   (version (string-drop (string-trim-right (get-string-all (open-pipe* OPEN_READ "git" "describe" "--long" "--dirty"))) 1))
   (source (local-file "../.." "ruby-gem-ext-zig_builder" #:recursive? #t))
   (build-system ruby-build-system)
   (native-inputs (list coreutils pkg-config which zig))
   (synopsis "")
   (description
    "")
   (home-page "")
   (license license:expat)))

ruby-gem-ext-zig_builder
