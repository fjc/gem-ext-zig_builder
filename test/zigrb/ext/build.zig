const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    ////////////////////////////////////////////////////////////////
    // lib

    const lib = b.addSharedLibrary(.{
        .name = "zigrb",
        .root_source_file = b.path("src/main.zig"),
        .target = target,
        .optimize = optimize,
    });

    b.installArtifact(lib);

    ////////////////////////////////////////////////////////////////
    // test

    const unit_tests = b.addTest(.{
        .root_source_file = b.path("src/main.zig"),
        .target = target,
        .optimize = optimize,
    });

    const run_unit_tests = b.addRunArtifact(unit_tests);
    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_unit_tests.step);

    ////////////////////////////////////////////////////////////////
    // mod

    const zigby_pkg = b.dependency("zigby", .{});
    const zigby_mod = zigby_pkg.module("zigby");

    lib.root_module.addImport("zigby", zigby_mod);
    unit_tests.root_module.addImport("zigby", zigby_mod);
}
