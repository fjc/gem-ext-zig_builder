const std   = @import("std");
const zigby = @import("zigby");
const ruby  = zigby.ruby;

fn ruby_init() void {
    const zig_rb_class = ruby.rb_define_class("ZigRb", ruby.rb_cObject);
    _ = ruby.rb_define_method(zig_rb_class, "add", rb_add, 2);
}

fn rb_add(...) callconv(.C) ruby.VALUE {
    var ap = @cVaStart();
    defer @cVaEnd(&ap);

    const self = @cVaArg(&ap, ruby.VALUE); _ = self;
    const arg1 = @cVaArg(&ap, ruby.VALUE);
    const arg2 = @cVaArg(&ap, ruby.VALUE);

    return ruby.INT2NUM(ruby.NUM2INT(arg1) + ruby.NUM2INT(arg2));
}

comptime {
    zigby.ruby_init("libzigrb", ruby_init);
}
