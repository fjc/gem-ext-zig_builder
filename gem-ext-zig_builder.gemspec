# frozen_string_literal: true

require_relative 'lib/gem/ext/zig_builder/version'

Gem::Specification.new do |spec|
  spec.name          = 'gem-ext-zig_builder'
  spec.version       = Gem::Ext::ZigBuilder::VERSION
  spec.authors       = ['Frank J. Cameron']
  spec.email         = ['fjc@fastmail.net']

  spec.summary       = 'Gem extension builder for zig'
  spec.description   = ''
  spec.homepage      = 'https://gitlab.com/fjc/gem-ext-zig_builder'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.5.0')

  spec.metadata['homepage_uri']    = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  spec.metadata['changelog_uri']   = spec.homepage

  spec.metadata['rubygems_mfa_required'] = 'true'

  spec.files         = Dir['lib/**/*.rb']
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
end
