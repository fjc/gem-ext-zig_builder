# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name = 'zigrb'
  spec.version = '0.0.1'
  spec.authors = ['Frank J. Cameron']
  spec.email = ['fjc@fastmail.net']

  spec.summary = 'An example gem for ruby with native zig extensions.'
  spec.homepage = 'https://gitlab.com/fjc/gem-ext-zig_builder/test/zigrb'
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 2.5.0'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage

  spec.files = Dir['lib/**/*.rb'] + Dir['ext/**/*.zig'] + Dir['ext/**/*.[ch]'] + Dir['ext/**/*.rb']
  spec.files.reject! { |fn| fn.include? 'zig-cache' }
  spec.files.reject! { |fn| fn.include? 'zig-out' }
  spec.require_paths = ['lib']
  spec.extensions = [
    'ext/build.zig',
    'ext/build.zig.zon',
  ]
end
