# frozen_string_literal: true

# RubyGems will load plugins in the latest version of each installed gem
# or $LOAD_PATH. Plugins must be named ‘rubygems_plugin’ (.rb, .so,
# etc) and placed at the root of your gem’s #require_path. Plugins are
# installed at a special location and loaded on boot.
# https://guides.rubygems.org/plugins/

require_relative 'gem/ext/zig_builder'
